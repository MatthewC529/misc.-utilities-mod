package com.mattc529.miscutil.proxy;

public interface IProxyable {

	public void registerTiles();
	public void registerBlocks();
	public void registerItems();
	public void initRender();
	
}
