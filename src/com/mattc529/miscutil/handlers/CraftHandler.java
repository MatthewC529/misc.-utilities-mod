package com.mattc529.miscutil.handlers;

import java.util.ArrayList;

import com.mattc529.miscutil.blocks.MiscBlock;
import com.mattc529.miscutil.util.Logger;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.FurnaceRecipes;

public class CraftHandler {
	
	public enum CraftType{
		SMELTING, SHAPED, SHAPELESS;
	}
	
	public static class CraftData{
		
		private CraftType craftStyle;
		private ItemStack result;
		private Object[] recipe;
		private float xp;
		private MiscBlock block = null;
		private Item item = null;
		private Integer metadata = null;
		
		public CraftData(CraftType craft, MiscBlock block, ItemStack result, Object... recipe){
			craftStyle = craft;
			this.block = block;
			this.result = result;
			this.recipe = recipe;
		}
		
		public CraftData(Item item, ItemStack result, Object... recipe){
			craftStyle = CraftType.SHAPED;
			this.item = item;
			this.result = result;
			this.recipe = recipe;
		}
		
		public CraftData(MiscBlock block, ItemStack result, float xp){
			craftStyle = CraftType.SMELTING;
			this.block = block;
			this.result = result;
			this.xp = xp;
		}
		
		public CraftData addMetadata(int metadata){
			this.metadata = metadata;
			return this;
		}
		
		public void registerRecipe(){
			
			if(craftStyle.equals(CraftType.SMELTING) && item == null){
				if(metadata != null){
					CraftHandler.addSmeltingRecipe(block, metadata, result, xp);
				}else{
					CraftHandler.addSmeltingRecipe(block, result, xp);
				}
			}else if(craftStyle.equals(CraftType.SHAPED)){
				CraftHandler.addShapedRecipe(result, recipe);
			}else if(craftStyle.equals(CraftType.SHAPELESS)){
				CraftHandler.addShapelessRecipe(result, recipe);
			}else return;
		}
	}
	
	private static ArrayList<CraftData> registerQueue = new ArrayList<CraftData>();
	private static FurnaceRecipes smelt = FurnaceRecipes.smelting();
	private static CraftingManager craft = CraftingManager.getInstance();
	
	private CraftHandler(){}
	
	public static void initRecipes(){
		for(CraftData data: registerQueue){
			data.registerRecipe();
			if(data.block != null) 	Logger.debug("CRAFT-REG -- " + (data.craftStyle.equals(CraftType.SHAPED) ? "shaped recipe for ": data.craftStyle.equals(CraftType.SHAPELESS)? "shapeless recipe for ":"smelting recipe for ") + data.block.getUnlocalizedName());
			else Logger.debug("CRAFT-REG -- " + (data.craftStyle.equals(CraftType.SHAPED) ? "shaped recipe for ": data.craftStyle.equals(CraftType.SHAPELESS)? "shapeless recipe for ":"smelting recipe for ") + data.item.getUnlocalizedName());
		}
		Logger.info("All Crafting Recipes Registered!");
	}
	
	public static void addShapedRecipe(ItemStack result, Object... recipe){
		craft.addRecipe(result, recipe);
	}
	
	public static void addShapelessRecipe(ItemStack result, Object... recipe){
		craft.addShapelessRecipe(result, recipe);
	}
	
	public static void addSmeltingRecipe(MiscBlock block, ItemStack result, float xp){
		smelt.addSmelting(block.blockID, result, xp);
	}
	
	public static void addSmeltingRecipe(MiscBlock block, int metadata, ItemStack result, float xp){
		smelt.addSmelting(block.blockID, metadata, result, xp);
	}
	
	public static void addRecipeData(CraftData data){
		registerQueue.add(data);
	}
	
}