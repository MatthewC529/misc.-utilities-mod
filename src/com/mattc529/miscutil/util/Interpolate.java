package com.mattc529.miscutil.util;

/**
 * 
 * Initial Finished Implementation of Interpolation Methods <br />
 * <br />
 * Takes a value between 0 and 1 as its Alpha and calculate the interpolated value using <br />
 * a specific function (Sinusoidal, Linear, Exponential, etc.).
 *
 */
public abstract class Interpolate {

	public static final Interpolate pow2 = new Power(2);
	public static final Interpolate pow2In = new PowerInto(2);
	public static final Interpolate pow2Out = new PowerOut(2);
	
	public static final Interpolate pow3 = new Power(3);
	public static final Interpolate pow3In = new PowerInto(3);
	public static final Interpolate pow3Out = new PowerOut(3);
	
	public static final Interpolate pow4 = new Power(4);
	public static final Interpolate pow4In = new PowerInto(4);
	public static final Interpolate pow4Out = new PowerOut(4);
	
	public static final Interpolate pow5 = new Power(5);
	public static final Interpolate pow5In = new PowerInto(5);
	public static final Interpolate pow5Out = new PowerOut(5);
	
	public static final Interpolate exp5 = new Exponential(2, 5);
	public static final Interpolate exp5In = new ExponentialInto(2, 5);
	public static final Interpolate exp5Out = new ExponentialOut(2, 5);
	
	public static final Interpolate exp10 = new Exponential(2, 10);
	public static final Interpolate exp10In = new ExponentialInto(2, 10);
	public static final Interpolate exp10Out = new ExponentialOut(2, 10);
	
	public static final Interpolate elastic = new Elastic(2f, 10f);
	public static final Interpolate elasticIn = new ElasticIn(2f, 10f);
	public static final Interpolate elasticOut = new ElasticOut(2f, 10f);
	
	public static final Interpolate swing = new Swing(1.5f);
	public static final Interpolate swingIn = new SwingIn(2f);
	public static final Interpolate swingOut = new SwingOut(2f);
	
	public static final Interpolate bounce = new Bounce(4);
	public static final Interpolate bounceIn = new BounceIn(4);
	public static final Interpolate bounceOut = new BounceOut(4);
	
	
	abstract public float apply(float a);
	
	public float apply(float start, float end, float a){
		return start + (end - start) * apply(a);
	}
	
	public static final Interpolate linear = new Interpolate(){
		@Override
		public float apply(float a){
			return a;
		}
	};
	
	public static final Interpolate fade = new Interpolate(){
		@Override
		public float apply(float a){
			return Maths.clamp(a*a*a*(a*(a*6 -15) + 10), 0, 1);
		}
	};
	
	public static final Interpolate sine = new Interpolate(){
		@Override
		public float apply(float a){
			return (1-Maths.cos(a * Maths.PI/2))/2;
		}
	};
	
	public static final Interpolate sineIn = new Interpolate(){
		@Override
		public float apply(float a){
			return 1 - Maths.cos(a * Maths.PI/2);
		}
	};
	
	public static final Interpolate sineOut = new Interpolate(){
		@Override
		public float apply(float a){
			return Maths.sin(a * Maths.PI/2);
		}
	};
	
	public static final Interpolate circle = new Interpolate(){
		@Override
		public float apply(float a){
			if (a <= 0.5f) {
				a *= 2;
				return (1 - Maths.sqrt(1 - a * a)) / 2;
			}
			a--;
			a *= 2;
			return (Maths.sqrt(1 - a * a) + 1) / 2;
		}
	};
	
	public static final Interpolate circleIn = new Interpolate(){
		@Override
		public float apply(float a){
			return 1 - Maths.sqrt(1 - a*a);
		}
	};
	
	public static final Interpolate circleOut = new Interpolate(){
		@Override
		public float apply(float a){
			a--;
			return Maths.sqrt(1 - a*a);
		}
	};
	
	// -- Power Interpolation Classes -- //
	
	public static class Power extends Interpolate{

		protected int power;
		
		public Power(int power){
			this.power = power;
		}
		
		@Override
		public float apply(float a) {
			if(a <= 0.5f) return Maths.pow(2*a, power)/2;
			else return Maths.pow((a - 1) * 2, power) / (power % 2 == 0 ? -2 : 2) + 1;
		}
		
	}
	
	public static class PowerInto extends Power{
		public PowerInto(int power){
			super(power);
		}
		
		@Override
		public float apply(float a){
			return Maths.pow(a, power);
		}
	}
	
	public static class PowerOut extends Power{
		public PowerOut(int power){
			super(power);
		}
		
		@Override
		public float apply(float a){
			return Maths.pow((a-1), power) * (power % 2 == 0 ? -1 : 1) + 1;
		}
	}
	
	//-- Exponential Interpolation Classes --//
	
	public static class Exponential extends Interpolate{
		final float value, power, min, scale;
		
		public Exponential(float value, int power){
			this.value = value;
			this.power = power;
			min = Maths.pow(value, -power);
			scale = 1 / (1-min);
		}
		
		@Override
		public float apply(float a){
			if (a <= 0.5f) return ((float)Math.pow(value, power * (a * 2 - 1)) - min) * scale / 2;
			return (2 - ((float)Math.pow(value, -power * (a * 2 - 1)) - min) * scale) / 2;
		}
		
	}
	
	public static class ExponentialInto extends Exponential{
		public ExponentialInto(float value, int power){
			super(value, power);
		}
		
		@Override
		public float apply(float a){
			return ((float)Math.pow(value, power * (a - 1)) - min) * scale;
		}
	}
	
	public static class ExponentialOut extends Exponential{
		public ExponentialOut(float value, int power){
			super(value, power);
		}
		
		@Override
		public float apply(float a){
			return 1 - ((float)Math.pow(value, -power * a) - min) * scale;
		}
	}
	
	//-- Elastic Interpolation Classes --//
	
	static public class Elastic extends Interpolate {
		final float value, power;

		public Elastic (float value, float power) {
			this.value = value;
			this.power = power;
		}

		@Override
		public float apply (float a) {
			if (a <= 0.5f) {
				a *= 2;
				return (float)Math.pow(value, power * (a - 1)) * Maths.sin(a * 20) * 1.0955f / 2;
			}
			a = 1 - a;
			a *= 2;
			return 1 - (float)Math.pow(value, power * (a - 1)) * Maths.sin((a) * 20) * 1.0955f / 2;
		}
	}

	static public class ElasticIn extends Elastic {
		public ElasticIn (float value, float power) {
			super(value, power);
		}

		@Override
		public float apply (float a) {
			return (float)Math.pow(value, power * (a - 1)) * Maths.sin(a * 20) * 1.0955f;
		}
	}

	static public class ElasticOut extends Elastic {
		public ElasticOut (float value, float power) {
			super(value, power);
		}

		@Override
		public float apply (float a) {
			a = 1 - a;
			return (1 - (float)Math.pow(value, power * (a - 1)) * Maths.sin(a * 20) * 1.0955f);
		}
	}

	//-- Bounce Interpolation Classes --//

	static public class Bounce extends BounceOut {
		public Bounce (float[] widths, float[] heights) {
			super(widths, heights);
		}

		public Bounce (int bounces) {
			super(bounces);
		}

		private float out (float a) {
			float test = a + widths[0] / 2;
			if (test < widths[0]) return test / (widths[0] / 2) - 1;
			return super.apply(a);
		}

		@Override
		public float apply (float a) {
			if (a <= 0.5f) return (1 - out(1 - a * 2)) / 2;
			return out(a * 2 - 1) / 2 + 0.5f;
		}
	}
	
	static public class BounceOut extends Interpolate {
		final float[] widths, heights;

		public BounceOut (float[] widths, float[] heights) {
			if (widths.length != heights.length)
				throw new IllegalArgumentException("Must be the same number of widths and heights.");
			this.widths = widths;
			this.heights = heights;
		}

		public BounceOut (int bounces) {
			if (bounces < 2 || bounces > 5) throw new IllegalArgumentException("bounces cannot be < 2 or > 5: " + bounces);
			widths = new float[bounces];
			heights = new float[bounces];
			heights[0] = 1;
			switch (bounces) {
			case 2:
				widths[0] = 0.6f;
				widths[1] = 0.4f;
				heights[1] = 0.33f;
				break;
			case 3:
				widths[0] = 0.4f;
				widths[1] = 0.4f;
				widths[2] = 0.2f;
				heights[1] = 0.33f;
				heights[2] = 0.1f;
				break;
			case 4:
				widths[0] = 0.34f;
				widths[1] = 0.34f;
				widths[2] = 0.2f;
				widths[3] = 0.15f;
				heights[1] = 0.26f;
				heights[2] = 0.11f;
				heights[3] = 0.03f;
				break;
			case 5:
				widths[0] = 0.3f;
				widths[1] = 0.3f;
				widths[2] = 0.2f;
				widths[3] = 0.1f;
				widths[4] = 0.1f;
				heights[1] = 0.45f;
				heights[2] = 0.3f;
				heights[3] = 0.15f;
				heights[4] = 0.06f;
				break;
			}
			widths[0] *= 2;
		}

		@Override
		public float apply (float a) {
			a += widths[0] / 2;
			float width = 0, height = 0;
			for (int i = 0, n = widths.length; i < n; i++) {
				width = widths[i];
				if (a <= width) {
					height = heights[i];
					break;
				}
				a -= width;
			}
			a /= width;
			float z = 4 / width * height * a;
			return 1 - (z - z * a) * width;
		}
	}

	static public class BounceIn extends BounceOut {
		public BounceIn (float[] widths, float[] heights) {
			super(widths, heights);
		}

		public BounceIn (int bounces) {
			super(bounces);
		}

		@Override
		public float apply (float a) {
			return 1 - super.apply(1 - a);
		}
	}

	//-- Swing Interpolation Classes --//

	static public class Swing extends Interpolate {
		private final float scale;

		public Swing (float scale) {
			this.scale = scale * 2;
		}

		@Override
		public float apply (float a) {
			if (a <= 0.5f) {
				a *= 2;
				return a * a * ((scale + 1) * a - scale) / 2;
			}
			a--;
			a *= 2;
			return a * a * ((scale + 1) * a + scale) / 2 + 1;
		}
	}

	static public class SwingOut extends Interpolate {
		private final float scale;

		public SwingOut (float scale) {
			this.scale = scale;
		}

		@Override
		public float apply (float a) {
			a--;
			return a * a * ((scale + 1) * a + scale) + 1;
		}
	}

	static public class SwingIn extends Interpolate {
		private final float scale;

		public SwingIn (float scale) {
			this.scale = scale;
		}

		@Override
		public float apply (float a) {
			return a * a * ((scale + 1) * a - scale);
		}
	}
}
