package com.mattc529.miscutil.util;

import java.util.Random;  

/**
 * Minecraft Math Class Adapted from EliteLib-LibGDX Edition Maths Class. <br />
 * <br />
 * Only dependency is java.util.Random... is a Bit Overkill
 */
public final class Maths {

	// Prevent Instantiation
	private Maths() {
	}

	/**
	 * Eulers Number, used in natural exponents and natural logs for natural
	 * growth, natural decay, compound interest and so on. Its all natural.
	 */
	public static final float E = 2.71828182F;

	/**
	 * Defined as Two Times the Ratio of a Circles Circumference to its
	 * Diameter.<br />
	 * Its main function is in trigonometric functions or physics where it
	 * represents a complete unit circle, following a logical reasoning. <br/>
	 * It represents 2pi so 2piD turns into tauD and formulae for the Period of
	 * a Spring or Pendulum repalce 2pi with tau... yes I am pushing an
	 * agenda...
	 */
	public static final float TAU = 6.283185302F;

	/**
	 * A rational compromise in the Pi vs Tau debate by Randall Munroe at <a
	 * href="http://xkcd.com/1292/">XKCD</a><br />
	 * Also its an awesome sounding word... and why not include it? 1.5*pi
	 */
	public static final float PAU = 4.71238898F;

	/** The ratio of a circles circumference to its diameter */
	public static final float PI = 3.14159265F;

	private static final float degreesToRadians = TAU / 360;
	private static final float degRad = degreesToRadians;
	private static final float radDeg = 180 / PI;

	private static final int SIN_BITS = 14;
	private static final int SIN_MASK = ~(-1 << SIN_BITS);
	private static final int SIN_COUNT = SIN_MASK + 1;
	private static final float degIndex = SIN_COUNT / 360;
	private static final float radIndex = SIN_COUNT / TAU;

	private static final float F_UPPER = 2048f;
	private static final float F_LOWER = -2048f;
	private static final double D_UPPER = 4096;
	private static final double D_LOWER = -4096;

	/**
	 * After hours of research I have learned a lot about working with bit-wise
	 * operators. <br />
	 * This is the sum of my labor, hours of work put into one beautiful class
	 * and a few methods
	 * 
	 * @author Matthew Crocco (Treehouse Elite) with Contributions from
	 *         StackOverflow (Various Users) and LibGDX OpenSource Code!
	 * 
	 */
	private static class Sine {
		protected static final float[] valTable = new float[SIN_COUNT];
		static {
			for (int i = 0; i < SIN_COUNT; i++)
				valTable[i] = (float) Math.sin((i + 0.5) / SIN_COUNT * TAU);
			for (int i = 0; i < 360; i += 90)
				valTable[(int) (i * degIndex) & SIN_MASK] = (float) Math.sin(i
						* degreesToRadians);
		}
	}

	/** Convert degrees to radians */
	public static float toRadians(float degrees) {
		return degrees * degRad;
	}

	/** Convert degrees to radians */
	public static float toRadians(int degrees) {
		return toRadians(degrees);
	}

	/** Convert radians to degrees */
	public static float toDegrees(float radians) {
		return radians * radDeg;
	}

	/** Convert radians to degrees */
	public static float toDegrees(int radians) {
		return toDegrees(radians);
	}

	/** Get the value of sin(radians) in a quick and efficient manner */
	public static float sin(float radians) {
		return Sine.valTable[(int) (radians * radIndex) & SIN_MASK];
	}

	/** Get the value of sin(degrees) in a quick and efficient manner */
	public static float sinD(float degrees) {
		return Sine.valTable[(int) (degrees * degIndex) & SIN_MASK];
	}

	/** Get the value of cos(radians) in a quick and efficient manner */
	public static float cos(float radians) {
		return Sine.valTable[(int) ((radians + PI / 2) * radIndex) & SIN_MASK];
	}

	/** Get value of cos(degrees) in a quick and efficient manner */
	public static float cosD(float degrees) {
		return Sine.valTable[(int) ((degrees + 90) * degIndex) & SIN_MASK];
	}

	public static float tan(float radians) {
		return sin(radians) / cos(radians);
	}

	public static float tanD(float degrees) {
		return sin(toRadians(degrees)) / cos(toRadians(degrees));
	}

	public static float sec(float radians) {
		return 1 / cos(radians);
	}

	public static float csc(float radians) {
		return 1 / sin(radians);
	}

	public static float cot(float radians) {
		return 1 / tan(radians);
	}

	// ----------------------------------------------------------------------

	private static final Random r = new Random(); // Decent Pseudorandom Number
													// Generator in my
													// opinion...

	/**
	 * Returns a random float between [0.0, 1.0)
	 */
	public static float random() {
		return r.nextFloat();
	}

	/**
	 * Returns a random integer between [0, range] inclusive
	 */
	public static int random(int range) {
		return r.nextInt(range + 1);
	}

	/**
	 * Returns a random integer between [start, end] inclusive
	 */
	public static int random(int start, int end) {
		return start + r.nextInt(end - start + 1);
	}

	/** Returns a random boolean value */
	public static boolean randomBool() {
		return r.nextBoolean();
	}

	/**
	 * Returns true if a random value falls below the given chance. False if
	 * equal to or greater than that chance.<br />
	 * Values must be between [0.0,1.0]
	 */
	public static final boolean randomBool(float chance) {
		return Maths.random() < chance;
	}

	/**
	 * Returns a random float between [0, range)
	 * 
	 * @param range
	 *            - max value (exclusive)
	 * @return Random Float
	 */
	public static final float random(float range) {
		return r.nextFloat() * range;
	}
	
	public static final double randomNormal(){
		return r.nextGaussian();
	}
	
	public static final double randomNormalRanged(double min, double max){
		return clamp(randomNormal() + min, min, max);
	}

	/**
	 * Get a random value between [start, end)
	 * 
	 * @param start
	 *            - start inclusive
	 * @param end
	 *            - end exclusive
	 * @return Random Float between those two values
	 */
	public static final float random(float start, float end) {
		return start + r.nextFloat() * (end - start);
	}

	// ----------------------------------------------------------------

	// Interesting Bit-Hack I suppose....
	/**
	 * Uses a common bit-hack to efficiently and quickly gather the next highest
	 * Power of Two
	 * 
	 * @param value
	 *            - starting value
	 * @return Next Power Of Two as an int
	 */
	public static final int nextPowerOfTwo(int value) {
		if (value == 0)
			return 1;

		value--;
		value |= value >> 1;
		value |= value >> 2;
		value |= value >> 4;
		value |= value >> 8;
		value |= value >> 16;
		return value + 1;
	}

	/**
	 * Uses some Bit-wise magic to efficiently return whether or not a value is
	 * a Power of Two (useful for animators and maps which require it!).<br />
	 * Particularly helpful for OpenGL where images are a power of two.
	 * 
	 * @param value
	 *            - value to test
	 * @return True if it is, false if not.
	 */
	public static final boolean isPowerOfTwo(int value) {
		return value != 0 && (value & value - 1) == 0;
	}

	// ----------------------------------------------------------------
	// So I was getting inaccurate values so I hit up StackOverflow and they
	// recommended the same values as LibGDX...
	// It still isnt clear to me and I look back seeing a lot of mimics of
	// LibGDX but the Bit stuff I can understand
	// and the Sine stuff but this seems arbitrary, I may still recommend
	// Math.ceil but this SHOULD work.

	private static final int BIGINT = 16 * 1024;
	private static final double BIGFLOOR = BIGINT;
	private static final double CEILING = 0.99999999;
	private static final double BIGCEIL = 16384.999999999996;
	private static final double BIGROUND = BIGINT + 0.5f;

	/**
	 * Floors a float value, all realistic values work fine but certain extreme
	 * values are affected by some inaccuracy.
	 */
	public static final int floors(float value) {
		return (int) (value + BIGFLOOR) - BIGINT;
	}

	/**
	 * Floors a float value, IT MUST BE POSITIVE, all realistic values work fine
	 * but extreme values can be somewhat inaccurate.
	 */
	public static final int floorPositive(float value) {
		return (int) value;
	}

	/**
	 * Uses the less efficient JNI interfacing Math.floor method from
	 * java.lang.Math and casts it to an int value.
	 * 
	 * @see java.lang.Math#floor(double) Math.floor(double)
	 */
	public static final int floor(float value) {
		int isolate = Integer.parseInt(Float.toString(value).substring(
				Float.toString(value).indexOf('.') + 1,
				Float.toString(value).indexOf('.') + 2));
		if (isolate >= 5)
			return (int) (value - 0.5);
		else
			return (int) value;
	}

	/**
	 * Gets the ceiling of a float value, all realistic values work fine but
	 * extreme values can be somewhat inaccurate.
	 */
	public static final int ceils(float value) {
		return (int) (value + BIGCEIL) - BIGINT;
	}

	/**
	 * Gets the ceiling of a float value, IT MUST BE POSITIVE, all realistic
	 * values work fine but extreme values can be somewhat inaccurate.
	 */
	public static final int ceilPositive(float value) {
		return (int) (value + CEILING);
	}

	/**
	 * Uses the less efficient, JNI implementing, Math.ceil() method to get the
	 * ceiling and then casts it to an int value.
	 * 
	 * @see java.lang.Math#ceil(double) Math.ceil(double)
	 */
	public static final int ceil(float value) {
		int isolate = Integer.parseInt(Float.toString(value).substring(
				Float.toString(value).indexOf('.') + 1,
				Float.toString(value).indexOf('.') + 2));
		if (isolate >= 5)
			return (int) value;
		else
			return (int) (value + 0.5);
	}

	/**
	 * Rounds either up or down to the nearest integer for any float value, all
	 * realistic values work fine but extreme values can be somewhat inaccurate.
	 */
	public static final long round(float value) {
		return (long) (value + BIGROUND) - BIGINT;
	}
	
	public static final long round(double value) {
		return (long)((long) (value + BIGROUND) - BIGROUND);
	}

	/**
	 * Rounds either up or down to the nearest integer for POSITIVE float
	 * values, all realistic values work fine but extreme values can be somewhat
	 * inaccurate.
	 */
	public static final long roundPositive(float value) {
		return (long) (value + 0.5f);
	}

	/**
	 * Round a Float to the nth decimal place (n = places)
	 * 
	 * @param value
	 *            - Value to round
	 * @param places
	 *            - Decimal places to round to
	 * @return Value rounded to places-th decimal place.
	 */
	public static final float round(float value, int places) {
		float scientific = 1 * (pow(10, places+1));
		long mutated = round(value * scientific);
		return mutated / scientific;
	}

	/**
	 * Round a Double to the nth decimal place (n = places)
	 * 
	 * @param value
	 *            - Value to round
	 * @param places
	 *            - Decimal places to round to
	 * @return Value rounded to places-th decimal place.
	 */
	public static final double round(double value, int places) {
		double scientific = 1 * (pow(10, places+1));
		long mutated = round(value * scientific);
		return mutated / scientific;
	}

	/**
	 * Raise a float to the nth power (n = power)<br />
	 * <br />
	 * Recently became able to support negative and decimal powers.
	 * 
	 * @param base
	 *            - Value to apply an exponent to
	 * @param power
	 *            - Power of the exponent
	 * @return Value raised to the power-th power
	 */
	public static final float pow(float base, int power) {
		if (power == 0)
			return 1;
		if (power == 1)
			return base;
		if (power == -1)
			return (1 / base);

		// NOW HANDLES NEGATIVES :D
		if (power < 0) {
			return (1 / base)
					* pow(base, (power < -1 ? power + 1 : power - power));
		} else {
			return base * pow(base, (power > 1 ? power - 1 : power - power));
		}
	}

	/**
	 * Raise a float to the nth power (n = power)
	 * 
	 * @param base
	 *            - Value to apply an exponent to
	 * @param power
	 *            - Power of the exponent
	 * @return Value raised to the power-th power
	 */
	public static final double pow(double base, int power) {
		if (power == 0)
			return 1;
		if (power == 1)
			return base;
		if (power == -1)
			return (1 / base);

		// NOW HANDLES NEGATIVES :D
		if (power < 0) {
			return (1 / base)
					* pow(base, (power < -1 ? power + 1 : power - power));
		} else {
			return base * pow(base, (power > 1 ? power - 1 : power - power));
		}
	}

	private static float orig = 0;
	private static float pastGuess = 0;

	/**
	 * Get the Square Root of Base and round to the Round-th decimal place. <br />
	 * <br />
	 * If you try to square root a negative you get an imaginary number, at the
	 * moment this yields a return value of -1... <br />
	 * <br />
	 * I mean... Technically that is correct for....... if you dont think too
	 * much.
	 * 
	 * @param base
	 *            - Value to get the square root of
	 * @param round
	 *            - Round to this decimal place. If this = 0 then dont round, if
	 *            this = -1 then round to the first integer.
	 * @return Square root of base
	 */
	public static float sqrt(float base, int round) {
		if (base < 0)
			return -1;
		if (base == 0)
			return 0;
		if (orig == 0) {
			orig = base;
			double guess = Math.sqrt(Maths.nextPowerOfTwo((int) base));
			float nextGuess = (float) ((guess + (orig / guess)) / 2);
			pastGuess = nextGuess;
			if (nextGuess * nextGuess == orig)
				return round > 0 || round == -1 ? (round == -1 ? round(
						nextGuess, 0) : round(nextGuess, round)) : nextGuess;
			else
				return sqrt(nextGuess, round);
		}
		float nextGuess = (base + (orig / base)) / 2;
		if (nextGuess == pastGuess) {
			orig = 0;
			pastGuess = 0;
			return round > 0 || round == -1 ? (round == -1 ? round(nextGuess, 0)
					: round(nextGuess, round))
					: nextGuess;
		} else {
			pastGuess = nextGuess;
			return sqrt(nextGuess, round);
		}
	}

	/**
	 * Get the Suare Root of Base unrounded <br />
	 * <br />
	 * If you try to square root a negative you get an imaginary number, at the
	 * moment this yields a return value of -1... <br />
	 * 
	 * @param base
	 *            - Base to get the square root of
	 * @return Square Root of base
	 */
	public static float sqrt(float base) {
		return sqrt(base, 0);
	}

	private static double exactOrig = 0;
	private static double exactPrevGuess = 0;

	/**
	 * Get the Square Root of Base and round to the Round-th decimal place. <br />
	 * <br />
	 * If you try to square root a negative you get an imaginary number, at the
	 * moment this yields a return value of -1... <br />
	 * <br />
	 * I mean... Technically that is correct for....... if you dont think too
	 * much.
	 * 
	 * @param base
	 *            - Value to get the square root of
	 * @param round
	 *            - Round to this decimal place. If this = 0 then dont round, if
	 *            this = -1 then round to the first integer.
	 * @return Square root of base
	 */
	public static double sqrt(double base, int round) {
		if (base < 0)
			return -1;
		if (base == 0)
			return 0;
		if (exactOrig == 0) {
			exactOrig = base;
			double guess = Math.sqrt(Maths.nextPowerOfTwo((int) base));
			float nextGuess = (float) ((guess + (exactOrig / guess)) / 2);
			exactPrevGuess = nextGuess;
			if (nextGuess * nextGuess == exactOrig)
				return round > 0 || round == -1 ? (round == -1 ? round(
						(double) nextGuess, 0) : round((double) nextGuess,
						round)) : nextGuess;
			else
				return sqrt((double) nextGuess, round);
		}
		double nextGuess = (base + (exactOrig / base)) / 2;
		if (nextGuess == exactPrevGuess) {
			exactOrig = 0;
			exactPrevGuess = 0;
			return round > 0 || round == -1 ? (round == -1 ? round(nextGuess, 0)
					: round(nextGuess, round))
					: nextGuess;
		} else {
			exactPrevGuess = nextGuess;
			return sqrt(nextGuess, round);
		}
	}

	/**
	 * Get the square root of base, unrounded <br />
	 * <br />
	 * If you try to square root a negative you get an imaginary number, at the
	 * moment this yields a return value of -1... <br />
	 * 
	 * @param base
	 *            - Base to get the square root of
	 * @return Square Root of Base
	 */
	public static double sqrt(double base) {
		return sqrt(base, 0);
	}

	/**
	 * finds the cube root of any reasonable number, at extremes extremely
	 * inaccurate values will be returned. [x^(1/3)] <br />
	 * <br />
	 * Actually parses a Float from a Double value that was converted to a
	 * String. <br />
	 * Process: 3rd Root of value as double -> String -> Float parsed from
	 * String <br />
	 * <br />
	 * This is done to maintain precision since the double method ends up being
	 * more accurate.
	 * 
	 * @param value
	 *            - Value to get the Cube Root of
	 * @return cube root of Value
	 */
	public static float cbrt(float value) {
		return Float.parseFloat(Double.toString(round(xroot(3, (double) value),
				6)));
	}

	/**
	 * Finds the Cube Root of any Reasonable Number, at extremes extremely
	 * inaccurate values will be returned. [x^(1/3)] <br />
	 * <br />
	 * Calls xroot(3, value)
	 * 
	 * @param value
	 * @return Cube Root of Value
	 */
	public static double cbrt(double value) {
		return xroot(3, value);
	}

	/**
	 * Calculates the xth root of any number. As long as the Root is a power of
	 * two (i.e. 2, 4, 8, 16, 32, etc.) <br />
	 * <br />
	 * Uses the fact that any root that is a power of two can be represented as
	 * that sqrt of the sqrt of the sqrt and so on. <br/>
	 * Example: 4th root of 2 = 2^(1/4) = (2^1/2^1/2) = sqrt(sqrt(2)) <br />
	 * Example: 16th root of 2 = 2^(1/16) = (2^1/2^1/2^1/2^1/2) =
	 * sqrt(sqrt(sqrt(sqrt(2)))) <br />
	 * <br />
	 * Currently negative roots are unsupported.
	 * 
	 * @param root
	 *            - Root to calculate
	 * @param val
	 *            - Value to derive root from
	 * @return The Xth Root of Val
	 */
	public static float rootPowOf2(int root, float val) {
		if (val < 0)
			return -1;
		if (isPowerOfTwo(root) && root != 1) {
			return rootPowOf2(root / 2, sqrt(val));
		} else {
			return val;
		}
	}

	/**
	 * Calculates the xth root of any number accurately. As long as the Root is
	 * a power of two (i.e. 2, 4, 8, 16, 32, etc.) <br />
	 * <br />
	 * Uses the fact that any root that is a power of two can be represented as
	 * that sqrt of the sqrt of the sqrt and so on. <br/>
	 * Example: 4th root of 2 = 2^(1/4) = (2^1/2^1/2) = sqrt(sqrt(2)) <br />
	 * Example: 16th root of 2 = 2^(1/16) = (2^1/2^1/2^1/2^1/2) =
	 * sqrt(sqrt(sqrt(sqrt(2)))) <br />
	 * <br />
	 * Currently negative roots are unsupported.
	 * 
	 * @param root
	 *            - Root to calculate
	 * @param val
	 *            - Value to derive root from
	 * @return The Xth Root of Val
	 */
	public static double rootPowOf2(int root, double val) {
		if (val < 0)
			return -1;
		if (isPowerOfTwo(root) && root != 1) {
			return rootPowOf2(root / 2, sqrt(val));
		} else {
			return val;
		}
	}

	static double prevTestD = Double.NaN;
	static float prevTestF = Float.NaN;

	/**
	 * Calculates the Xth root of Val between upper and lower using a Binary
	 * Search Algorithm. <br />
	 * <br />
	 * Called by cbrt and xroot. <br />
	 * <br />
	 * If root < 0 returns Not A Number for Float <br />
	 * If root = 0 returns Positive Infinity for Float <br />
	 * If root = 1 returns val <br />
	 * If root = 2 returns sqrt(val) -- More Accurate <br />
	 * If root = 3 returns cbrt(val) -- More Accurate <br />
	 * 
	 * @param root
	 *            - Root to Obtain
	 * @param val
	 *            - Value to get the root Root of
	 * @param upper
	 *            - Upper Limit (Default uses F_UPPER)
	 * @param lower
	 *            - Lower Limit (Default uses F_LOWER)
	 * @return Xth Root of Val
	 */
	private static float calcFloatRoot(int root, float val, float upper,
			float lower) {

		if (root < 0)
			return Float.NaN;
		if (root == 0)
			return Float.POSITIVE_INFINITY;
		if (root == 1)
			return val;
		if (root == 2)
			return sqrt(val);
		if (root == 3)
			return cbrt(val); // Added due to the weakness of calcFloatRoot

		float mid = (upper + lower) / 2;
		float test = pow(mid, root);

		if (test == val || test == prevTestF) {
			prevTestF = Float.NaN;
			// If we hit the Float Upper Bound try to parse a Float out of
			// calcDoubleRoot
			// It has a much higher success rate
			if (mid == F_UPPER || mid == F_LOWER)
				return Float.parseFloat(Double.toString(round(
						calcDoubleRoot(root, val, D_UPPER, D_LOWER), 6)));
			else
				return mid;
		} else if (test > val) {
			prevTestF = test;
			return calcFloatRoot(root, val, mid, lower);
		} else if (test < val) {
			prevTestF = test;
			return calcFloatRoot(root, val, upper, mid);
		} else {
			prevTestF = Float.NaN;
			return mid;
		}
	}

	/**
	 * Calculates the Xth root of Val between upper and lower using a Binary
	 * Search Algorithm. <br />
	 * <br />
	 * Called by cbrt and xroot. <br />
	 * <br />
	 * This method ends up being more accurate and precise than calcFloatRootM<br />
	 * <br />
	 * If root < 0 returns Not A Number for Double <br />
	 * If root = 0 returns Positive Infinity for Double <br />
	 * If root = 1 returns val <br />
	 * If root = 2 returns sqrt(val) -- More Accurate <br />
	 * 
	 * @param root
	 *            - Root to Obtain
	 * @param val
	 *            - Value to get the root Root of
	 * @param upper
	 *            - Upper Limit (Default uses D_UPPER)
	 * @param lower
	 *            - Lower Limit (Default uses D_LOWER)
	 * @return Xth Root of Val
	 */
	private static double calcDoubleRoot(int root, double val, double upper,
			double lower) {
		if (root < 0)
			return Double.NaN;
		if (root == 0)
			return Double.POSITIVE_INFINITY;
		if (root == 1)
			return val;
		if (root == 2)
			return sqrt(val);

		double mid = (upper + lower) / 2;
		double test = pow(mid, root);

		if (test == val || test == prevTestD) {
			prevTestD = Double.NaN;
			return mid;
		} else if (test > val) {
			prevTestD = test;
			return calcDoubleRoot(root, val, mid, lower);
		} else if (test < val) {
			prevTestD = test;
			return calcDoubleRoot(root, val, upper, mid);
		} else {
			prevTestD = Double.NaN;
			return mid;
		}

	}

	/**
	 * Returns the Xth root of Val <br />
	 * <br />
	 * If root < 0 returns Float.NaN <br />
	 * If root = 0 returns Float.POSITIVE_INFINITY <br />
	 * If root = 1 returns val <br />
	 * If root = 2 returns sqrt(val) -- More Accurate <br />
	 * If root = 3 returns cbrt(val) -- More Accurate <br />
	 * <br />
	 * If xth root is obviously inaccurate, derive Float from a Double. (See
	 * calcFloatRoot) <br />
	 * 
	 * 
	 * @param root
	 *            - Root to derive
	 * @param val
	 *            - Value to get the root of
	 * @return Xth Root of Val
	 */
	public static float xroot(int root, float val) {
		return calcFloatRoot(root, val, F_UPPER, F_LOWER);
	}

	/**
	 * Returns the Xth Root of Val <br />
	 * <br />
	 * If root < 0 returns Double.NaN <br />
	 * If root = 0 returns Double.POSITIVE_INFINITY <br />
	 * If root = 1 returns val <br />
	 * If root = 2 returns sqrt(val) -- More Accurate <br />
	 * 
	 * @param root
	 * @param val
	 * @return
	 */
	public static double xroot(int root, double val) {
		return calcDoubleRoot(root, val, D_UPPER, D_LOWER);
	}

	/**
	 * Get the absolute value of any number.
	 * 
	 * @param val
	 * @return Absolute Value of Val
	 */
	public static float abs(float val) {
		if (val < 0)
			return -val;
		else
			return val;
	}

	/**
	 * Get the absolute value of any number.
	 * 
	 * @param val
	 * @return Absolute Value of Val
	 */
	public static double abs(double val) {
		if (val < 0)
			return -val;
		else
			return val;
	}
	
	/**
	 * Get the absolute value of any number.
	 * 
	 * @param val
	 * @return Absolute Value of Val
	 */
	public static long abs(long val){
		if(val < 0)
			return -val;
		else
			return val;
	}
	
	/**
	 * Get the absolute value of any number.
	 * 
	 * @param val
	 * @return Absolute Value of Val
	 */
	public static int abs(int val){
		if(val < 0)
			return -val;
		else
			return val;
	}

	/**
	 * Get the decimal log of any number. <br />
	 * <br />
	 * Works using the Change of Base Formula with the Natural Logarithm. <br />
	 * Such that log10(5) = ln(5)/ln(10)
	 * 
	 * @param val
	 * @return Decimal Logarithm of Val
	 */
	public static double log10(double val) {
		if (val == 1)
			return 0;
		if (val == 10)
			return 1;
		return (Math.log(val) / Math.log(10));
	}

	/**
	 * Get the decimal log of any number. <br />
	 * <br />
	 * Works using the Change of Base Formula with the Natural Logarithm. <br />
	 * Such that log10(5) = ln(5)/ln(10)
	 * 
	 * @param val
	 * @return Decimal Logarithm of Val
	 */
	public static float log10(float val) {
		if (val == 1)
			return 0;
		if (val == 10)
			return 1;
		return (float) (Math.log(val) / Math.log(10));
	}

	/**
	 * Gets the natural log of any number.
	 * 
	 * @param val
	 * @return Natural Logarithm of Val
	 */
	public static double ln(double val) {
		if (val == 1)
			return 0;
		// Added since passing my value for Eulers Number caused an
		// output of 0.9999...94 due to accuracy loss associated
		// with float <-> double conversions.
		if (val == E || val == Math.E)
			return 1;
		return Math.log(val);
	}

	/**
	 * Gets the natural log of any number.
	 * 
	 * @param val
	 * @return Natural Logarithm of Val
	 */
	public static float ln(float val) {
		if (val == 1)
			return 0;
		// Added since passing my value for Eulers Number caused an
		// output of 0.9999...94 due to accuracy loss associated
		// with float <-> double conversions.
		if (val == E || val == Math.E)
			return 1;
		return (float) Math.log(val);
	}

	/**
	 * Get the base logarithm of any number. <br />
	 * <br />
	 * Works using the Change of Base Formula with the Natural Logarithm.<br />
	 * Such that log3.7(12) = ln(12)/ln(3.7)
	 * 
	 * @param base
	 * @param val
	 * @return The base logarithm of val
	 */
	public static double logx(double base, double val) {
		if (val == 1)
			return 0;
		if (val == base)
			return 1;
		return ln(val) / ln(base);
	}

	/**
	 * Get the base logarithm of any number. <br />
	 * <br />
	 * Works using the Change of Base Formula with the Natural Logarithm.<br />
	 * Such that log3.7(12) = ln(12)/ln(3.7)
	 * 
	 * @param base
	 * @param val
	 * @return The base logarithm of val
	 */
	public static float logx(float base, float val) {
		if (val == 1)
			return 0;
		if (val == base)
			return 1;
		return ln(val) / ln(base);
	}

	/**
	 * Calculates The Hypotenuse/Resultant Vector of given X and Y Components
	 * 
	 * @param x
	 *            - X Component
	 * @param y
	 *            - Y Component
	 * @return Resultant Vector/Hypotenuse
	 */
	public static float hypot(float x, float y) {
		return sqrt(pow(x, 2) + pow(y, 2));
	}

	/**
	 * Calculates The Hypotenuse/Resultant Vector of given X and Y Components
	 * 
	 * @param x
	 *            - X Component
	 * @param y
	 *            - Y Component
	 * @return Resultant Vector/Hypotenuse
	 */
	public static double hypot(double x, double y) {
		return sqrt(pow(x, 2) + pow(y, 2));
	}

	/**
	 * Calculates Resultant Vector of given X and Y Components
	 * 
	 * @param x
	 *            - X Component
	 * @param y
	 *            - Y Component
	 * @return Resultant Vector/Hypotenuse
	 */
	public static float calcMagnitude(float x, float y) {
		return hypot(x, y);
	}

	/**
	 * Calculates Resultant Vector of given X and Y Components
	 * 
	 * @param x
	 *            - X Component
	 * @param y
	 *            - Y Component
	 * @return Resultant Vector/Hypotenuse
	 */
	public static double calcMagnitude(double x, double y) {
		return hypot(x, y);
	}
	
	/*public static float calcMagnitude(Vector2D vector){
		return hypot(vector.x, vector.y);
	}*/

	/**
	 * Calculates the Magnitude of a 3D Vector
	 * @param vector
	 * @return Magnitude of vector
	 */
	public static double calcMagnitude(double x, double y, double z){
		return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}
	
	/**
	 * Clamp a Value between Min and Max
	 * @param val - Value to clamp
	 * @param min - Minumum
	 * @param max - Maximum
	 * @return Scaled Value
	 */
	public static double clamp(double val, double min, double max){
		if(val > max) return max;
		if(val < min) return min;
		return val;
	}
	
	/**
	 * Clamp a Value between Min and Max
	 * @param val - Value to clamp
	 * @param min - Minumum
	 * @param max - Maximum
	 * @return Scaled Value
	 */
	public static float clamp(float val, float min, float max){
		if(val > max) return max;
		if(val < min) return min;
		return val;
	}
	
	/**
	 * Clamp a Value between Min and Max
	 * @param val - Value to clamp
	 * @param min - Minumum
	 * @param max - Maximum
	 * @return Scaled Value
	 */
	public static long clamp(long val, long min, long max){
		if(val > max) return max;
		if(val < min) return min;
		return val;
	}
	
	/**
	 * Clamp a Value between Min and Max
	 * @param val - Value to clamp
	 * @param min - Minumum
	 * @param max - Maximum
	 * @return Scaled Value
	 */
	public static int clamp(int val, int min, int max){
		if(val > max) return max;
		if(val < min) return min;
		return val;
	}
	
	/**
	 * Clamp a Value between Min and Max
	 * @param val - Value to clamp
	 * @param min - Minumum
	 * @param max - Maximum
	 * @return Scaled Value
	 */
	public static short clamp(short val, short min, short max){
		if(val > max) return max;
		if(val < min) return min;
		return val;
	}
	
	/**
	 * Normalize a Value to between 0 and 1. <br />
	 * if val > max return 1 <br />
	 * if val < min return 0 <br />
	 * else return scaled value. <br />
	 * @param val - Value to Normalize
	 * @param min - Minimum Value
	 * @param max - Maximum Value
	 * @return
	 */
	public static float normalize(float val, float min, float max){
		if(val > max) return 1;
		if(val < min) return 0;
		return (val - min)/(max-min);
	}
	
	/**
	 * Normalize a Value to between 0 and 1. <br />
	 * if val > max return 1 <br />
	 * if val < min return 0 <br />
	 * else return scaled value. <br />
	 * @param val - Value to Normalize
	 * @param min - Minimum Value
	 * @param max - Maximum Value
	 * @return
	 */
	public static float normalize(int val, int min, int max){
		if(val > max) return 1;
		if(val < min) return 0;
		return (val - min)/(max-min);
	}
	
	/**
	 * Normalize a Value to between 0 and 1. <br />
	 * if val > max return 1 <br />
	 * if val < min return 0 <br />
	 * else return scaled value. <br />
	 * @param val - Value to Normalize
	 * @param min - Minimum Value
	 * @param max - Maximum Value
	 * @return
	 */
	public static double normalize(double val, double min, double max){
		if(val > max) return 1;
		if(val < min) return 0;
		return (val - min)/(max-min);
	}
	
	/**
	 * Normalize a Value to between 0 and 1. <br />
	 * if val > max return 1 <br />
	 * if val < min return 0 <br />
	 * else return scaled value. <br />
	 * @param val - Value to Normalize
	 * @param min - Minimum Value
	 * @param max - Maximum Value
	 * @return
	 */
	public static float normalize(long val, long min, long max){
		if(val > max) return 1;
		if(val < min) return 0;
		return (val - min)/(max-min);
	}
	
	/**
	 * Normalize a Value to between 0 and 1. <br />
	 * if val > max return 1 <br />
	 * if val < min return 0 <br />
	 * else return scaled value. <br />
	 * @param val - Value to Normalize
	 * @param min - Minimum Value
	 * @param max - Maximum Value
	 * @return
	 */
	public static float normalize(short val, short min, short max){
		if(val > max) return 1;
		if(val < min) return 0;
		return (val - min)/(max-min);
	}
	
}
