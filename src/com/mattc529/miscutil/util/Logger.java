package com.mattc529.miscutil.util;

import java.util.logging.Level; 

import com.mattc529.miscutil.ref.Ref;

import cpw.mods.fml.common.FMLLog;

/**
 * Misc Utilities Logger Class to interface with FMLLog.
 */
public class Logger {
	
	private static java.util.logging.Logger muLogger = java.util.logging.Logger.getLogger(Ref.LOG_NAME);
	private static Logger instance = null;
	private static boolean initialized = false;
	
	private Logger(){}
	
	public static Logger getInstance(){
		if(instance == null){
			if(!initialized) Logger.init();
			instance = new Logger();
		}
		
		return instance;
	}
	
	public static void init(){
		if(initialized) return;
		
		initialized = true;
		muLogger.setParent(FMLLog.getLogger());
		
		info(Ref.DEBUG ? "Debug Mode is Enabled!" : "Debug Mode is Not Enabled!");
	}
	
	public static void log(Object msg, Level logLevel){
		muLogger.log(logLevel, String.valueOf(msg));
	}
	
	public static void severe(Object msg){
		log(msg, Level.SEVERE);
	}
	
	public static void warning(Object msg){
		log(msg, Level.WARNING);
	}
	
	public static void info(Object msg){
		log(msg, Level.INFO);
	}
	
	public static void debug(Object msg){
		if(!Ref.DEBUG) return;
		log(String.format("[MU_DEBUG] -- %s", String.valueOf(msg)), Level.INFO);
	}
	
	public static void config(Object msg){
		log(msg, Level.CONFIG);
	}
	
	public static void fine(Object msg){
		log(msg, Level.FINE);
	}
	
	public static void finer(Object msg){
		log(msg, Level.FINER);
	}
	
	public static void finest(Object msg){
		log(msg, Level.FINEST);
	}
	
	
}
