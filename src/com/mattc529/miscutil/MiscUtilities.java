package com.mattc529.miscutil;

import com.mattc529.miscutil.blocks.MiscBlock;
import com.mattc529.miscutil.handlers.CraftHandler;
import com.mattc529.miscutil.handlers.GUIHandler;
import com.mattc529.miscutil.items.MiscItem;
import com.mattc529.miscutil.proxy.CommonProxy;
import com.mattc529.miscutil.ref.Ref;
import com.mattc529.miscutil.util.Logger;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;


@Mod(modid=Ref.MOD_ID,name=Ref.MOD_NAME,version=Ref.VERSION,dependencies=Ref.DEPENDENCIES,certificateFingerprint=Ref.FINGERPRINT)
public class MiscUtilities {
	
	@Instance(Ref.MOD_ID)
	public static MiscUtilities instance;
	
	@SidedProxy(clientSide = Ref.CLIENT, serverSide = Ref.SERVER, modId = Ref.MOD_ID)
	public static CommonProxy proxy;
	
	public static Logger logger;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent evt){
		
		logger = Logger.getInstance();
		MiscBlock.initBlocks();
		MiscItem.initItems();
		CraftHandler.initRecipes();
	}
	
	@EventHandler
	public void init(FMLInitializationEvent evt){
		
		NetworkRegistry.instance().registerGuiHandler(instance, new GUIHandler());
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent evt){
		
	}
	
}
