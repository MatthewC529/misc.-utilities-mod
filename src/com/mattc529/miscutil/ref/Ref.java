package com.mattc529.miscutil.ref;

public final class Ref {
	
	private Ref(){}
	
	public static final String MOD_ID = "miscutil";
	public static final String MOD_NAME = "Misc. Utilities";
	public static final String LOG_NAME = "Misc-Utils";
	public static final String VERSION = "Pre-Alpha";
	public static final String DEPENDENCIES = "required-after:Forge@[9.11.1.965,)";
	public static final String FINGERPRINT = "@FINGERPRINT@";
	public static final String AUTHOR = "Mattthew C. (Dr_HouseMD)";
	
	public static final String CLIENT = "com.mattc529.miscutil.proxy.ClientProxy";
	public static final String SERVER = "com.mattc529.miscutil.proxy.ServerProxy";
	
	public static final boolean DEBUG = true;
	
}
