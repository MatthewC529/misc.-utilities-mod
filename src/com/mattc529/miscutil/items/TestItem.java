package com.mattc529.miscutil.items;

import net.minecraft.item.ItemStack;

import com.mattc529.miscutil.handlers.CraftHandler;
import com.mattc529.miscutil.handlers.CraftHandler.CraftData;

public class TestItem extends MiscItem implements ICraftableItem<TestItem> {

	public TestItem(int id, CraftData recipe) {
		super(id, StackSize.ONE, recipe);
		this.setUnlocalizedName("testItem");
	}
	
	public TestItem(int id){
		this(id, null);
	}

	@Override
	public TestItem addSmelting(ItemStack result, float xp) {
		return this;
	}

	@Override
	public TestItem addSmelting(int metadata, ItemStack result, float xp) {
		return this;
	}

	@Override
	public TestItem addRecipe(int resultAmount,Object... recipe) {
		CraftHandler.addRecipeData(new CraftData(this, new ItemStack(this, resultAmount), recipe));
		return this;
	}

	@Override
	public void register() {
		MiscItem.registerItem(new ItemWrapper(this, "Test Item"));
	}
	
}
