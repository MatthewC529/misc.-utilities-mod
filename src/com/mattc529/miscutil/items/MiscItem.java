package com.mattc529.miscutil.items;

import java.util.ArrayList;

import com.mattc529.miscutil.handlers.CraftHandler;
import com.mattc529.miscutil.handlers.CraftHandler.CraftData;
import com.mattc529.miscutil.ref.Constants;
import com.mattc529.miscutil.ref.ItemID;
import com.mattc529.miscutil.ref.Ref;
import com.mattc529.miscutil.util.Logger;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public abstract class MiscItem extends Item {

	public enum StackSize{
		ONE(1),
		THIRTY_TWO(32),
		SIXTEEN(16),
		SIXTY_FOUR(64);
		
		private int size = 0;
		
		private StackSize(int stackSize){
			size = stackSize;
		}
		
		public int getSize(){
			return size;
		}
		
	}
	
	private static ArrayList<ItemWrapper> wrappers = new ArrayList<ItemWrapper>();
	
	public static final int CORRECTION = 256;
	private boolean registered = false;
	
	public MiscItem(int id, StackSize size, CraftData recipe) {
		super(id - 256);
		this.maxStackSize = size.getSize();
		if(!registered){ this.register(); registered = true;}
		if(recipe != null){
			CraftHandler.addRecipeData(recipe);
		}
	}
	
	@Override
    public String getUnlocalizedName(){
        return String.format("item.%s%s", Constants.Strings.RES_PREFIX, getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
    }

    @Override
    public String getUnlocalizedName(ItemStack itemStack){
        return String.format("item.%s%s", Constants.Strings.RES_PREFIX, getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
    }

    protected String getUnwrappedUnlocalizedName(String unlocalizedName){
        return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister iconRegister){
        itemIcon = iconRegister.registerIcon(this.getUnlocalizedName().substring(this.getUnlocalizedName().indexOf(".") + 1));
    }

    protected static void registerItem(ItemWrapper wrap){
    	wrappers.add(wrap);
    }
    
    public static void initItems(){
    	
    	//ITEM INSTANTIATION
    	new TestItem(ItemID.TEST_TOOL_DEF).addRecipe(1,  "DDD", " S ", " S ", Character.valueOf('D'), Block.dirt, Character.valueOf('S'), Item.stick);
    	//--
    	
    	Logger.info("Instantiated All Items!");
    	for(ItemWrapper itemwrap: wrappers){
    		
    		Item item = itemwrap.getItem();
    		String name = itemwrap.getItemName();
    		
    		GameRegistry.registerItem(item, name, Ref.MOD_ID);
    		Logger.info("REGISTERED -- " + item.getUnlocalizedName());
    	}
    	
    }
    
    public abstract void register();
    
}
