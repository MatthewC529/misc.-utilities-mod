package com.mattc529.miscutil.items;

import net.minecraft.item.Item;  
import net.minecraft.item.ItemStack;
 
public interface ICraftableItem<T extends Item> {

	/**
	 * Adds a Smelting Recipe for the specified Block. Assumes no Metadata.
	 * @param result - ItemStack to give in return.
	 * @param xp - XP to give in return.
	 * @return Self for Method Chaining
	 */
	public T addSmelting(ItemStack result, float xp);
	
	/**
	 * Adds a Smelting Recipe for the specified Block.
	 * @param metadata - Metadata for Smelt
	 * @param result - ItemStack to give in return
	 * @param xp - XP to receive from furnace.
	 * @return Self for Method Chaining
	 */
	public T addSmelting(int metadata, ItemStack result, float xp);
	
	/**
	 * Adds a Crafting Recipe for the specified Block, supports Shaped and Shapeless
	 * @param craft - Shapeless or Shaped
	 * @param resultAmount - Amount of Block to Give to Player
	 * @param recipe - Recipe for the Given Craft
	 * @return Self to allow method chaining
	 */
	public T addRecipe(int resultAmount, Object... recipe);
	
}
