package com.mattc529.miscutil.items;

import net.minecraft.item.Item;

import com.mattc529.miscutil.handlers.CraftHandler.CraftData;
import com.mattc529.miscutil.util.Logger;

public class ItemWrapper {

	private static int curItemNum = 1;
	
	private Item item;
	private String name;
	private int id;
	
	/**
	 * Wraps Item Information.
	 * @param item - Item to Wrap
	 * @param name - Name of Item
	 * @param data - CraftData to Wrap with Item (Nullable)
	 */
	public ItemWrapper(Item item, String name){
		if(item == null || name == null){
			Logger.severe("#ERROR# -- Attempt to Create an Item Wrapper with a NULL Item or Name Parameter! -- RESOLVE: Not Loading Item #"+curItemNum+"!");
			throw new IllegalStateException("Attempt To Create an ItemWrapper with a NULL Item or Name Parameter! -- Item Number " + curItemNum);
		}
		
		this.item = item;
		this.name = name;
		this.id = item.itemID;
		
		curItemNum++;
	}
	
	/**
	 * Wraps Item Information. This Constructor attempts to assign a name. <br />
	 * <br />
	 * It first checks if the current unlocalizedName == "null", if true then it assigns a default name, else it uses the unlocalized name.
	 * @param item - Item to Wrap
	 * @param data - CraftData to Wrap with Item (Nullable)
	 */
	public ItemWrapper(MiscItem item){
		this(item, item.getUnlocalizedName().equalsIgnoreCase("null") ? "undefinedItemName"+curItemNum : item.getUnlocalizedName());
	}	
	
	public Item getItem(){
		return item;
	}
	
	public String getItemName(){
		return name;
	}
	
	public int getID(){
		return id;
	}
}
