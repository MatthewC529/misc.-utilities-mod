package com.mattc529.miscutil.blocks;

import net.minecraft.item.ItemStack;

import com.mattc529.miscutil.handlers.CraftHandler;
import com.mattc529.miscutil.handlers.CraftHandler.CraftData;
import com.mattc529.miscutil.handlers.CraftHandler.CraftType;

public class TestBlock extends MiscBlock implements ICraftableBlock<TestBlock> {

	public TestBlock(int id, CraftData recipe) {
		super(id, recipe);
		this.setUnlocalizedName("testBlock");
		this.setHardness(6);
		this.setLightValue(0.3f);
	}
	
	public TestBlock(int id){
		this(id, null);
	}
	
	@Override
	public TestBlock addSmelting(ItemStack result, float xp){
		hasSmeltingRecipe = true;
		CraftHandler.addRecipeData(new CraftData(this, result, xp));
		return this;
	}
	
	@Override
	public TestBlock addSmelting(int metadata, ItemStack result, float xp){
		hasSmeltingRecipe = true;
		CraftHandler.addRecipeData(new CraftData(this, result, xp).addMetadata(metadata));
		return this;
	}
	
	@Override
	public TestBlock addRecipe(CraftType craft, int resultAmount, Object... recipe){
		CraftHandler.addRecipeData(new CraftData(craft, this, new ItemStack(this, resultAmount), recipe));
		return this;
	}

	@Override
	public void register() {
		registerBlock(new BlockWrapper(this, null, this.getUnlocalizedName()));
	}

}
