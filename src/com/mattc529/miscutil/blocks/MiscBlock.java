package com.mattc529.miscutil.blocks;

import java.util.ArrayList; 

import com.mattc529.miscutil.handlers.CraftHandler;
import com.mattc529.miscutil.handlers.CraftHandler.CraftData;
import com.mattc529.miscutil.handlers.CraftHandler.CraftType;
import com.mattc529.miscutil.ref.BlockID;
import com.mattc529.miscutil.ref.Constants;
import com.mattc529.miscutil.util.Logger;
import com.mattc529.miscutil.util.Maths;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

/**
 * Base Class for all Misc Utilities Blocks. <br />
 * <br />
 * To Register a Block, the Sub-Block must be wrapped by the custom BlockWrapper class and added to the register list.
 *
 */
public abstract class MiscBlock extends Block {
	
	public static ArrayList<BlockWrapper> registerBlocks = new ArrayList<BlockWrapper>();
	
	protected boolean registered = false;
	protected boolean hasSmeltingRecipe = false;
	
	/**
	 * Creates a Block with the given ID and Material and then adds it to the Register Queue.
	 * @param id -- Reserved Block ID
	 * @param mat -- Block Material
	 */
	public MiscBlock(int id, Material mat, CraftData recipe) {
		super(id, mat);
		if(!registered){ this.register(); registered = true;}
		if(recipe != null){
			CraftHandler.addRecipeData(recipe);
		}
	}
	
	public MiscBlock(int id, CraftData data){
		this(id, Material.rock, data);
	}
	
	public MiscBlock(int id, Material mat){
		this(id, mat, null);
	}
	
	/**
	 * Defaults Block to use the given ID and the Rock material type
	 * @param id -- Reserved Block ID
	 */
	public MiscBlock(int id){
		this(id, Material.rock);
	}

	@Override
	public String getUnlocalizedName(){
		return String.format("block.%s%s", Constants.Strings.RES_PREFIX, getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister icon){
		this.blockIcon = icon.registerIcon(getUnwrappedUnlocalizedName(this.getUnlocalizedName()));
	}
	
	protected String getUnwrappedUnlocalizedName(String localizedName){
		return localizedName.substring(localizedName.indexOf('.') + 1);
	}
	
	//TODO
	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack){
		super.onBlockPlacedBy(world, x, y, z, entity, stack);
	}
	
	@Override
	public void breakBlock(World world, int x, int y, int z, int id, int meta){
		dropInv(world, x, y, z);
		super.breakBlock(world, x, y, z, id, meta);
	}
	
	protected void dropInv(World world, int x, int y, int z){
		
		TileEntity tile = world.getBlockTileEntity(x, y, z);
		
		if(!(tile instanceof IInventory)) return;
		
		IInventory inv = (IInventory) tile;
		
		for(int i = 0; i < inv.getSizeInventory(); i++){
			ItemStack stack = inv.getStackInSlot(i);
			
			if(stack != null && stack.stackSize > 0){
				
				float dX = Maths.random(-0.3f, 1);
				float dY = Maths.random(0.3f, 1);
				float dZ = Maths.random(-0.3f, 1);
				
				EntityItem droppedItem = new EntityItem(world, x + dX, y + dY, z + dZ, stack);
				
				if(stack.hasTagCompound()){
					droppedItem.getEntityItem().setTagCompound((NBTTagCompound)stack.getTagCompound().copy());
				}
				
				droppedItem.motionX = Maths.randomNormal() *.03;
				droppedItem.motionY = Maths.randomNormal() *.05;
				droppedItem.motionZ = Maths.randomNormal() *.03;
				world.spawnEntityInWorld(droppedItem);
				stack.stackSize = 0;
				
			}
		}
	}
	
	protected static void registerBlock(BlockWrapper wrapper){
		registerBlocks.add(wrapper);
	}
	
	public static void initBlocks(){
		
		//TODO Assemble Config -- Use Current ID's
		
		//-- Block Instantiations
		new TestBlock(BlockID.TEST_BLOCK_DEF).addRecipe(CraftType.SHAPELESS, 1, new ItemStack(Block.dirt));			//Testing Block
		//-- Block Instantiations
		
		Logger.info("Instantiated All Blocks! Woo!");
		for(BlockWrapper wrap: registerBlocks){									//Start Registering All Wrapped Blocks
			MiscBlock block;
			block = wrap.getBlock();
			Class<? extends ItemBlock> itemClass = wrap.getItemClass();
			String name = wrap.getBlockName();
			
			if(itemClass != null){
				GameRegistry.registerBlock(block, itemClass, "block."+name);	//Name Application Done Separately
			}else{
				GameRegistry.registerBlock(block, "block."+name);
			}
			Logger.info("REGISTERED -- " + block.getUnlocalizedName());
		}																		//Stop Registering Wrapped Blocks
	}
	
	public abstract void register();
	
}
