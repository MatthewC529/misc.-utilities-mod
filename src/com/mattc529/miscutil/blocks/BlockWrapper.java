package com.mattc529.miscutil.blocks;

import com.mattc529.miscutil.util.Logger;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;

public class BlockWrapper {
	
	public static int currentBlockNum = 1;
	private MiscBlock block;
	private Class<? extends Block> blockClass;
	private Class<? extends ItemBlock> itemClass;
	private String blockName;
	private int blockId;
	
	
	public BlockWrapper(MiscBlock block, Class<? extends ItemBlock> item, String name){
		if(block == null || name == null){
			Logger.severe("#ERROR# -- Attempt to Create a Block Wrapper with a NULL Block or Name Parameter! -- RESOLVE: Not Loading Block #"+currentBlockNum+"!");
			throw new IllegalStateException("Attempt To Create a BlockWrapper with a NULL Block or Name Parameter! -- Block Number " + currentBlockNum);
		}
		this.block = block;
		this.blockClass = block.getClass();
		this.blockId = block.blockID;
		this.itemClass = item;
		this.blockName = name;
		currentBlockNum++;
	}
	
	public MiscBlock getBlock(){
		return block;
	}
	
	public Class<? extends ItemBlock> getItemClass(){
		return itemClass;
	}
	
	public Class<? extends Block> getBlockClass(){
		return blockClass;
	}
	
	public int getBlockId(){
		return blockId;
	}
	
	public String getBlockName(){
		return blockName;
	}
	
}
