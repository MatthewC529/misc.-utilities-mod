package com.mattc529.miscutil.blocks;

import net.minecraft.item.ItemStack;

import com.mattc529.miscutil.handlers.CraftHandler.CraftType;

public interface ICraftableBlock<T extends MiscBlock> {

	/**
	 * Adds a Smelting Recipe for the specified Block. Assumes no Metadata.
	 * @param result - ItemStack to give in return.
	 * @param xp - XP to give in return.
	 * @return Self for Method Chaining
	 */
	public T addSmelting(ItemStack result, float xp);
	
	/**
	 * Adds a Smelting Recipe for the specified Block.
	 * @param metadata - Metadata for Smelt
	 * @param result - ItemStack to give in return
	 * @param xp - XP to receive from furnace.
	 * @return Self for Method Chaining
	 */
	public T addSmelting(int metadata, ItemStack result, float xp);
	
	/**
	 * Adds a Crafting Recipe for the specified Block, supports Shaped and Shapeless
	 * @param craft - Shapeless or Shaped
	 * @param resultAmount - Amount of Block to Give to Player
	 * @param recipe - Recipe for the Given Craft
	 * @return Self to allow method chaining
	 */
	public T addRecipe(CraftType craft, int resultAmount, Object... recipe);
	
}
